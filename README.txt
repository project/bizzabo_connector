CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Provides a module to connect Bizzabo API for listing events.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/bizzabo_connector   


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Bizzabo connector module as you would normally install a contributed
   Drupal module. Visit
   https://www.drupal.org/project/bizzabo_connector for further information.


CONFIGURATION
-------------

  1. Navigate to `Administration > Extend` and enable the module.
  2. Navigate to `Administration > Configuration > bizabo Client Configuration` for configuration.


MAINTAINERS
-----------

 * Vivek Rajavelu (Vivekraj) - https://www.drupal.org/u/vivekraj
