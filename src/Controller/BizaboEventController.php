<?php

namespace Drupal\bizzabo_connector\Controller;

use Drupal\bizzabo_connector\UtilityHelper;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Pager\PagerManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Bizzabo - API Controller.
 */
class BizaboEventController extends ControllerBase {

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * Constructs an OverviewTerms object.
   *
   * @param \Drupal\Core\Pager\PagerManagerInterface $pagerManager
   *   The pager manager.
   */
  public function __construct(PagerManagerInterface $pagerManager) {
    $this->pagerManager = $pagerManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('pager.manager')
    );
  }

  /**
   * Fetch Bizaboo API results.
   */
  public static function fetchApiResults($end_point, $post_data = NULL) {

    $config = \Drupal::config('bizzabo_connector.baseurl');
    $access_token = $config->get('auth_key');

    $response_array = [];

    $client = new Client([
      'base_url' => $end_point,
      'allow_redirects' => TRUE,
    ]);

    try {
      if ($post_data) {
        $response_obj = $client->post($end_point, [
          'body' => "",
          'headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'bearer ' . $access_token,
          ],
          'form_params' => $post_data,
        ]);
      }
      else {
        $response_obj = $client->get($end_point, [
          'body' => "",
          'headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'bearer ' . $access_token,
          ],
        ]);
      }
      $code = $response_obj->getStatusCode();

      if ($code === 200) {
        $response_array = Json::decode($response_obj->getBody());
        return $response_array;
      }
    }
    catch (RequestException $e) {
      if ($e->hasResponse()) {
        return $e->getMessage();
      }
    }
    return $response_array;
  }

  /**
   * Get params from API.
   */
  public static function getParams() {
    $config = \Drupal::config('bizzabo_connector.baseurl');
    $end_point = $config->get('bizabooapibaseurl');
    $events = self::fetchApiResults($end_point);

    if (count($events) > 0) {
      $params = $events['content'][0];
      return UtilityHelper::bizaboconfigList($params);
    }
  }

  /**
   * To Display events.
   */
  public static function getDisplayEvents() {
    $config = \Drupal::config('bizzabo_connector.baseurl');
    $end_point = $config->get('bizabooapibaseurl');
    $events = self::fetchApiResults($end_point);

    $bizabooResponse = self::returnPagerForArray($events['content'], 10);

    foreach ($bizabooResponse as $key => $nodedata_row) {
      $fullDate = date('m-d', strtotime($nodedata_row['startDate']));
      $fullwithYear = date('Y-m-d', strtotime($nodedata_row['startDate']));
      $fullYear = date('Y', strtotime($nodedata_row['startDate']));
      $fullmonth = date('m', strtotime($nodedata_row['startDate']));
      $fullmonthF = date('M', strtotime($nodedata_row['endDate']));

      $bizabooResponseRows[$key] = [
        'description' => $nodedata_row['name'],
        'field_start_date' => date('d', strtotime($nodedata_row['startDate'])),
        'field_end_date' => date('d', strtotime($nodedata_row['endDate'])),
        'month' => date('F', strtotime($nodedata_row['startDate'])),
        'field_start_time' => date('H:i', strtotime($nodedata_row['startDate'])),
        'field_end_time' => date('H:i', strtotime($nodedata_row['endDate'])),
        'location' => $nodedata_row['venue']['city'] . ',' . $nodedata_row['venue']['state'],
        'fullDateFormat' => $fullDate,
        'year' => $fullYear,
        'timezone' => $nodedata_row['timezone'],
        'DatewithYear' => $fullwithYear,
        'Fmonth' => $fullmonth,
        'fullmonthF' => $fullmonthF,
        'ís_node' => 0,
        'class' => 'bizabEvents',
      ];
    }

    $renderable['grid'] = [
      '#theme' => 'BizaboEventsDisplayPage',
      '#filter' => '',
      '#events' => $bizabooResponseRows,
      '#days' => '',
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    $renderable['pager'] = ['#type' => 'pager'];

    return $renderable;
  }

  /**
   * Testing API conection from backend.
   */
  public function testConnection() {
    print_r($form['bizabo_config_list']['#markup']);
    echo '<div style="margin:3%;display:block;text-align:center;"><input style="" type="button" value="Done" onClick="save_and_done();"></div>';
    exit();
  }

  /**
   * Chunk the response into number of item to display.
   */
  public static function returnPagerForArray($items, $num_page) {
    // Get total items count.
    $total = count($items);
    // Get the number of the current page.
    $current_page = \Drupal::service('pager.manager')->createPager($total, $num_page)->getCurrentPage();
    // Split an array into chunks.
    $chunks = array_chunk($items, $num_page);
    // Return current group item.
    $current_page_items = $chunks[$current_page];
    return $current_page_items;
  }

}
