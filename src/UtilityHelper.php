<?php

namespace Drupal\bizzabo_connector;

/**
 * Utility Helper for Bizabo manager.
 */
class UtilityHelper {

  /**
   * Config List for Bizabo.
   */
  public static function bizaboconfigList($params) {
    if ($params) {
      foreach ($params as $attr => $val) {
        $response_val = !is_array($val) && strlen($val) > 30 ? self::maskPattern(substr($val, 0, 30) . "...") : $val;
        $rows .= '<tr><td>' . $attr . '</td><td class=""><a class="" target="_blank"> ' . $response_val . '</a></td></tr>';
      }

      echo '<div class="" style="font-family: sans-serif;font-size: 15px;letter-spacing: 3px;">
              <div><p>Hello User, your API test connection is Successful. please find the below params from the API Response.</p></div>
              <table class="" style="border: 1px solid;" align="center">
                  <thead>
                      <tr>
                        <th class="" colspan="2">Response Params</th>
                        <th class="">Response Values</th>
                      </tr>
                  </thead>
                  <tbody> ' . $rows . '</tbody>
              </table>
          </div>';
      exit();
    }
  }

  /**
   * Helper function to Masking the response.
   */
  public static function maskPattern($string) {
    return substr($string, 0, 3) . "******" . substr($string, (strlen($string)) - 3, 3);
  }

}
