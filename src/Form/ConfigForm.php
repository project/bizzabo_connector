<?php

namespace Drupal\bizzabo_connector\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Bizzabo API Config Form.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bizzabo_connector.baseurl',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'baseurl_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bizzabo_connector.baseurl');

    $form['bizabooapibaseurl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Baseurl For The Event API'),
      '#required' => TRUE,
      '#default_value' => $config->get('bizabooapibaseurl'),
    ];
    $form['access_token'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Access Token Inputs'),
    ];
    $form['access_token']['auth_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Authorization Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('auth_key'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('bizzabo_connector.baseurl')
      ->set('bizabooapibaseurl', $form_state->getValue('bizabooapibaseurl'))
      ->set('auth_key', $form_state->getValue('auth_key'))
      ->save();
  }

}
