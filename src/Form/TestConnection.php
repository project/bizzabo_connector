<?php

namespace Drupal\bizzabo_connector\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Testing the API from Config Form.
 */
class TestConnection extends FormBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configfactory;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bizabo_mapping';
  }

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configfactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $bizzaboConfig = $this->config('bizzabo_connector.baseurl');
    $bizzaboUrl = $bizzaboConfig->get('bizabooapibaseurl');
    $bizzaboKey = $bizzaboConfig->get('auth_key');

    if (!empty($bizzaboUrl) && !empty($bizzaboKey)) {
      $id = "testConfigButton";
      $baseUrlValue = "/admin/bizabo/fetch/events/params";
    }

    $form['access_token']['test_connection'] = [
      '#value' => $this->t('Test Connection'),
      '#markup' => '<span id="base_Url" name="base_Url" data="' . $baseUrlValue . '"></span>
                                <a id="' . $id . '" class="button button--primary">Test Connection</a>',
    ];
    $form['#attached']['library'][] = 'bizzabo_connector/TestConfig';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
